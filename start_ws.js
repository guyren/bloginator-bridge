import inspect from 'object-inspect'

// Outgoing via Websocket
import { v4 as uuidv4 } from 'uuid'

import WebSocket, { WebSocketServer } from 'ws'
const wss = new WebSocketServer({ port: 7071 })

const clients = new Map()

wss.on('connection', (ws) => {
  const id = uuidv4()
  const color = Math.floor(Math.random() * 360)
  const metadata = { id, color }

  clients.set(ws, metadata)

  ws.on('message', (messageAsString) => {
    const message = JSON.parse(messageAsString)
    const metadata = clients.get(ws)

    message.sender = metadata.id
    message.color = metadata.color
    const outbound = JSON.stringify(message);

    [...clients.keys()].forEach((client) => {
      client.send(outbound)
    })
  })

  ws.on("close", () => {
    clients.delete(ws)
  })

  console.log("wss up")
})

// ---------------------

// Incoming via Postgres query
import pg_pkg from 'pg'

pg_pkg.native

const { Pool, Client } = pg_pkg

const pool = new Pool({
  user: 'bloginator',
  host: 'localhost',
  database: 'bloginator',
  password: '',
  port: 5432,
})

import pgIPC from 'pg-ipc'
var client = await pool.connect()
var ipc = pgIPC(client)

ipc.on('error', console.error)

var process_id = uuidv4()

ipc.on(process_id, (msg) => {
  console.log(msg)
})

ipc.notify(process_id, "TEST")